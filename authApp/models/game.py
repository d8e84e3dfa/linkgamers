from django.db import models

class Game(models.Model):
    codigo_juego = models.AutoField(primary_key=True)
    nombre_juego = models.CharField('Nombre del juego', max_length=200)
    tipo_juego = models.CharField('Tipo de juego', max_length=200)