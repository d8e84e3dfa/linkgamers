from django.db import models
from .user import User


class Perfil(models.Model):
    codigo_perfil = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, related_name='perfil', on_delete=models.CASCADE)
    linkFoto = models.CharField('LinkFoto', max_length=300)
    presentacion= models.CharField('Presentacion', max_length=200)